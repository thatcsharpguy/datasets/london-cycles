import geopandas as gpd
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from datetime import datetime
from matplotlib.animation import FuncAnimation

def plot_map(product, upstream):
    shapefile = gpd.read_file(str(upstream["map"]["shp"])).to_crs(epsg=4326)
    stations = pd.read_csv(str(upstream["cycles_info"]["stations"]))
    stats = pd.read_csv(str(upstream["cycles_info"]["stats"]))
    stats['proportion'] = (stats['docks'] - stats['empty_docks']) / stats['docks']

    min_y = stations["lat"].min() - 0.005
    max_y = stations["lat"].max() + 0.005

    min_x = stations["lon"].min() - 0.005
    max_x = stations["lon"].max() + 0.005

    fig = plt.figure(figsize=(6,4),dpi=170)
    ax = fig.gca()

    merged = stats.merge(stations)

    shapefile.plot(ax=ax)
    ax.set_ylim((min_y, max_y))
    ax.set_xlim((min_x, max_x))

    sns.scatterplot(y='lat', x='lon', hue='proportion', palette="Blues", data=merged, ax=ax, s=25)

    ax.axis('off')
    ax.get_legend().remove()
    ax.set_title("London cycles")

    fig.tight_layout()
    fig.patch.set_facecolor('white')

    plt.savefig(str(product))
