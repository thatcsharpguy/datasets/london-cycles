#!/usr/bin/env bash

wget https://data.london.gov.uk/download/statistical-gis-boundary-files-london/08d31995-dd27-423c-a987-57fe8e952990/London-wards-2018.zip
unzip London-wards-2018.zip
mkdir -p data
mv London-wards-2018_ESRI/London_Ward.shp {{product['shp']}}
mv London-wards-2018_ESRI/London_Ward.shx {{product['shx']}}
mv London-wards-2018_ESRI/London_Ward.cpg {{product['cpg']}}
mv London-wards-2018_ESRI/London_Ward.dbf {{product['dbf']}}
mv London-wards-2018_ESRI/London_Ward.prj {{product['prj']}}
mv London-wards-2018_ESRI/London_Ward.GSS_CODE.atx {{product['atx']}}
rm -rf London-wards-2018_ESRI London-wards-2018_Mapinfo London-wards-2018_ESRI.zip London-wards-2018.zip

