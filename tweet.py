from twython import Twython
import shutil
from datetime import datetime
import os

app_key = os.environ["API_KEY"]
app_secret = os.environ["API_SECRET"]
oauth_token = os.environ["ACCESS_TOKEN"]
oauth_token_secret = os.environ["ACCESS_TOKEN_SECRET"]
twitter = Twython(app_key, app_secret, oauth_token, oauth_token_secret)


def tweet(product, upstream):
    now = datetime.now().strftime("%m/%d/%Y, %H:%M")
    with open(upstream["plot"], "rb") as cycles_png:
        image = twitter.upload_media(media=cycles_png)
    twitter.update_status(
        status=f'London Cycles update at {now}',
        media_ids=[image['media_id']]
    )
    shutil.copy(upstream["plot"], product)