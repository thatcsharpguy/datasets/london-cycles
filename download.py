from tfl.api import bike_point
import pandas as pd
from datetime import datetime
import os

# + tags=["parameters"]
upstream = None
product = None
# -


def get_number(additional_properties, key):
    [nb] = [prop.value for prop in additional_properties if prop.key == key] 
    return int(nb)

def download_cycles_info(product):
    all_bike_points = bike_point.all()
    query_time = datetime.now()
    data = []

    for place in all_bike_points:
        bikes = get_number(place.additionalProperties, 'NbBikes')
        empty_docks = get_number(place.additionalProperties, 'NbEmptyDocks')
        docks = get_number(place.additionalProperties, 'NbDocks')
        data.append((place.id, place.commonName, place.lat, place.lon, bikes, empty_docks, docks))

    data_df = pd.DataFrame(data, columns=['id', 'name', 'lat', 'lon', 'bikes', 'empty_docks', 'docks']).set_index("id")
    data_df["query_time"] = pd.to_datetime(query_time).floor('Min')

    stations = data_df[[ "name", "lat", "lon"]].sort_index()
    stats = data_df[["bikes", "empty_docks", "docks", "query_time"]].sort_index()

    stations.to_csv(product['stations'])

    stats_file_name = product['stats']
    fresh_file = not os.path.exists(stats_file_name)

    stats.to_csv(stats_file_name, mode='a', header=fresh_file)
